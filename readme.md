Le Bon Spy
==========

## Description

Un set de scripts permettant une veille d'un célèbre site de petites annonces.

## Objectif

Une fois lancé, le script vérifie toutes les minutes les annonces. Lorsqu'une nouvelle annonce apparait, il télécharge l'annonce, les images, récupère le numéro de téléphone et envoie un email avec ce contenu. Il garde une copie de toutes les annonces et un registre lorsqu'elles ont été mise en ligne, supprimée, remise en lignes.

## Mise en place

À la première exécution, le script installe le fichier de paramêtres dans ~/.config/LeBonSpy.conf qu'il faut modifier par la suite avec les informations nécessaire à la gestion des emails.  
Doit l'email depuis laquelle le script envoie les courriels, le mot de passe, l'adresse email de réception (elle peut être identique) ainsi que l'adresse du protocole SMTP à utiliser.

Pour une facilité d'utilisation, il est possible d'utiliser screen pour démarrer le script dans un terminal, quitter le terminal sans arrêter le script.  
Par exemple, pour une mise en place sur un serveur distant :
```
    ssh user@domaine -p port
    cd ~/outils/
    git clone https://framagit.org/Jybz/lebonspy
    cd ./lebonspy
    screen -RDU lebonspy
    ./AlertAds.sh
    vi ~/.config/lebonspy.conf
    (Editer le fichier avec les bons paramètres)
    vi ./GetAds.sh
    (Editer le fichier avec les filtres de recherche)
    ./AlertAds.sh
    (Démarrage du script)
    (Détachez le script :)
    [CTRL]+[A]
    [D]
    exit
```
Exemple pour reprendre le script en cours :
```
    ssh user@domaine -p port
    screen -RDU lebonspy
    (...)
    [CTRL]+[A]
    [D]
    exit
```

## Limites

Le script a longuement dérivé d'un besoin et n'a pas été agencé pour l'affinage des recherches. Si le fichier de paramètre est "ailleurs", c'était pour éviter de rendre publique les adresses emails et mots de passes associés.  
Pour changer les paramètres de recherche, il faut modifier le fichier GetAds.sh, dans la section "Initialisation".
