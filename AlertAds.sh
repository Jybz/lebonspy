#!/bin/bash

export PATH="${PATH}:/usr/local/bin:/usr/local/games:/usr/games:${HOME}/.local/bin:${HOME}/bin"

cd $(dirname ${0})
source ./VARIABLES
SAVEPATH="./ads"

SENDATSTART=1

####################
## Initialisation ##
####################

REFRESH_TIME="1m"
MAIL_CLIENT="s-nail"
MAIL_CLIENT="mailx"
#export LC_ALL="C" 
 export LC_ALL="UTF-8" 
#export LC_CTYPE="fr_FR.UTF-8"
#export LANG="fr_FR.UTF-8"
PIC_PREFIX="PIC-"

CONF_FILE="/home/${USER}/.config/LeBonSpy.conf"

####################
## Program Start  ##
####################

hash jq
if [ 0 -ne ${?} ]; then
    echo "jq missing."
    exit 1
fi
hash ${MAIL_CLIENT}
if [ 0 -ne ${?} ]; then
    echo "${MAIL_CLIENT} missing."
    exit 1
fi

if [ -e ${CONF_FILE} ]; then
    CONFIG=$(cat ${CONF_FILE} | jq . )
    if [ 0 -ne ${?} ]; then
        echo -e "The configuration file is not proper formated.\nIf you delete it, the script will regenerate a default one." >&2
        exit 1
    fi
else
    echo -e "{\n  \"email\":\"mail@provider.tld\",\n  \"password\":\"yourSecretPassword\",\n  \"receiver\":\"YourOwnMail@yourProvider.tld\",\n  \"SMTP\":\"smtp://smtp.provider.tld:587\"\n}" 1>${CONF_FILE}
    echo "Configure your credentials first : ${CONF_FILE}" >&2
    exit 1
fi

if [ ! -e ${SAVEPATH} ]; then
    mkdir ${SAVEPATH}
    echo "Create a directory to save ads." >&2
elif [ ! -d ${SAVEPATH} ]; then
    echo "Directory to save ads is a file. Delete the file and create the directory." >&2
    rm -rf ${SAVEPATH}
    mkdir ${SAVEPATH}
fi

echo "debug segment"

EMAIL=$(echo "${CONFIG}" | jq --raw-output '.email')
PASS=$(echo "${CONFIG}" | jq --raw-output '.password')
DESTINATAIRE=$(echo "${CONFIG}" | jq --raw-output '.receiver')
SUBJECT="Alerte LBC"

SMTP_STARTTLS="smtp-use-starttls"
SMTP_SERVER=$(echo "${CONFIG}" | jq --raw-output '.SMTP')
SMTP_AUTH="login"
SMTP_USER="$(echo "${EMAIL}" | sed 's/@.*$//' )"
SMTP_PASS="${PASS}"

TMP_HTML=$(mktemp)
COOKIES=$(mktemp)

PRE_LIST=""
NEW_LIST=""

#Touche :
curl -c ${COOKIES} -b ${COOKIES} "https://www.leboncoin.fr" -H "${USER_AGENT}" -H "${ACCEPTED_LANGUAGE}" --compressed -o ${TMP_HTML} 2>/dev/null
ERR=${?}
#Récupération de APP_CONFIG
APP_CONFIG=$(cat ${TMP_HTML} | sed -e "/[[:space:]]*window\.APP_CONFIG/,/<\/script>/!d" -e "/<\/script>/d" -e "s/^[[:space:]]*window.APP_CONFIG[[:space:]]*=[[:space:]]*//" -e "/^[[:space:]]*$/d" )

API=$(echo "${APP_CONFIG}" | jq '."API"' )
JSON_KEY=$(echo "${API}" | jq --raw-output '."KEY_JSON"' )
API_KEY=$(echo "${API}" | jq --raw-output '."KEY"' )

# PRE_LIST=$(./GetAds.sh | grep list_id)
# NEW_LIST=${PRE_LIST}

Blocked=true
while [ ${Blocked} = true ]; do
    ALL_LIST=$(./GetAds.sh | jq '.' )
    echo "${ALL_LIST}" | jq 'del(.ads)'
    if [ -n "$(echo ${ALL_LIST} | sed -e '/^[[:space:]]*$/d' )" ]; then
        echo "Non empty content."
        if [ "null" != "$(echo ${ALL_LIST} | jq --raw-output '.total')" ]; then
            echo "Non blocked !"
#             NEW_LIST="$( echo ${ALL_LIST} | grep list_id)"
            NEW_LIST="${ALL_LIST}"
            Blocked=False
        else
            UNBLOCK_URL=$(echo ${ALL_LIST} | jq --raw-output '.url' )
            echo ${UNBLOCK_URL}
            curl -L -c ${COOKIES} -b ${COOKIES} ${UNBLOCK_URL} -H "${USER_AGENT}" -H "${ACCEPTED_LANGUAGE}" --compressed -o ${HOME}/return
            sleep 5m
        fi
    else
        sleep 5m
    fi
done

echo "Processing ads at start"
echo "${NEW_LIST}"

#Get all currently online ads
LIST=$(echo "${NEW_LIST}" | jq '.' | grep "list_id" | sed "s/.* \([[:digit:]]*\),/\1/g")
for i in ${LIST}; do
    echo "Process ads ${i}." >&2
    if [ ! -e "${SAVEPATH}/${i}" ]; then
        echo -e "\tAdd ads ${i}." >&2
        mkdir ${SAVEPATH}/${i}
        
        ANNONCE=$(curl -b ${COOKIES} "https://api.leboncoin.fr/finder/classified/${i}" -H "${USER_AGENT}" -H 'Accept: */*' -H "${ACCEPTED_LANGUAGE}" --compressed -H "api_key: ${API_KEY}" -H 'Content-Type: application/json' 2>/dev/null )
        echo "${ANNONCE}" >${SAVEPATH}/${i}/ads.json
        date '+%Y-%m-%d %H:%M:%S' >>${SAVEPATH}/${i}/date.added
        if [ "true" == $(echo "${ANNONCE}" | jq '.has_phone') ]; then
            echo -e "\t\tGet tel number." >&2
            NUMERO_TEL=$(curl -b ${COOKIES} 'https://api.leboncoin.fr/api/utils/phonenumber.json' -H "${USER_AGENT}" -H 'Accept: application/json' -H "${ACCEPTED_LANGUAGE}" --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Connection: keep-alive' --data "app_id=leboncoin_web_utils&key=${JSON_KEY}&list_id=${i}&text=1" 2>/dev/null | jq --raw-output '."utils"."phonenumber"' )
            echo "${NUMERO_TEL}" >>${SAVEPATH}/${i}/telephone
        else
            echo "null" >>${SAVEPATH}/${i}/telephone
            NUMERO_TEL=""
        fi
        touch "${SAVEPATH}/${i}/ONLINE"
        CMD_IMG=""
        if [ 0 -ne $(echo "${ANNONCE}" | jq '."images"."nb_images"' ) ]; then
            LIST_IMG=$(echo "${ANNONCE}" | jq --raw-output '."images"."urls_large"[]' )
            for j in ${LIST_IMG}; do
                IMG_NAME=$(echo "${j}" | sed -e 's/^.*\/\([[:alnum:]]*.jpg\)/\1/' )
                if [ ! -e "${SAVEPATH}/${i}/${PIC_PREFIX}${IMG_NAME}" ]; then
                    echo -e "\t\tGet picture ${PIC_PREFIX}${IMG_NAME}." >&2
                    curl ${j} -o ${SAVEPATH}/${i}/${PIC_PREFIX}${IMG_NAME} 2>/dev/null
                fi
                CMD_IMG="-a ${SAVEPATH}/${i}/${PIC_PREFIX}${IMG_NAME} ${CMD_IMG}"
            done
        fi
        
        if [ 1 -eq ${SENDATSTART} ]; then
            PRIX=$(echo "${ANNONCE}" | jq '."price"[0]')
            SURFACE=$(echo "${ANNONCE}" | jq --raw-output '."attributes"[] | select(."key"=="square")."value"' )
            if [ ${SURFACE} -gt 0 ] ; then
                PRIX_M2=$((${PRIX}/${SURFACE}))
            else
                PRIX_M2="NC"
            fi
            URL=$(echo "${ANNONCE}" | jq --raw-output '."url"' )
            SALER=$(echo "${ANNONCE}" | jq --raw-output '."owner"."name"' )
            MESSAGE=$(echo -e "Annonce : ${i}\n${URL}\nTéléphone : ${NUMERO_TEL}\nNom : ${SALER}\n\nPrix : ${PRIX}\nSurface : ${SURFACE}\nPrix au m^2 : ${PRIX_M2}\n---\n\n$(echo "${ANNONCE}" | jq '.body')\n---" )
            SUBJECT="Alerte LBC - $(echo "${ANNONCE}" | jq --raw-output '."subject"' )"
    #             echo -e ${MAIL_CLIENT} -n -s "${SUBJECT}" ${CMD_IMG} -S sendcharsets="UTF-8" -S ttycharset="UTF-8" -S from=${EMAIL} -S ${SMTP_STARTTLS} -S smtp=${SMTP_SERVER} -S ${SMTP_AUTH} -S smtp-auth-user="${SMTP_USER}" -S smtp-auth-password="${SMTP_PASS}" ${DESTINATAIRE}
            echo -e "${MESSAGE}" | ${MAIL_CLIENT} -n -s "${SUBJECT}" ${CMD_IMG} -S sendcharsets="UTF-8" -S ttycharset="UTF-8" -S from=${EMAIL} -S ${SMTP_STARTTLS} -S smtp=${SMTP_SERVER} -S ${SMTP_AUTH} -S smtp-auth-user="${SMTP_USER}" -S smtp-auth-password="${SMTP_PASS}" ${DESTINATAIRE}
        fi
        
    else
        if [ -e "${SAVEPATH}/${i}/OFFLINE" ]; then
            mv "${SAVEPATH}/${i}/OFFLINE" "${SAVEPATH}/${i}/ONLINE"
        fi
    fi
done

for i in $( ls ${SAVEPATH}/ ); do
    if [ -e "${SAVEPATH}/${i}/ONLINE" ]; then
        if [ 0 -eq $(echo "${LIST}" | grep -c "${i}") ]; then
            echo "Ads ${i} removed." >&2
            mv "${SAVEPATH}/${i}/ONLINE" "${SAVEPATH}/${i}/OFFLINE"
        fi
    elif [ ! -e "${SAVEPATH}/${i}/OFFLINE" ]; then
        touch "${SAVEPATH}/${i}/OFFLINE"
    fi
done

while [ ${ERR} -eq 0 ]; do
    echo "new Check. $(date '+%Y-%m-%d %H:%M:%S')"
    PRE_LIST=${NEW_LIST}
    ALL_LIST=$(./GetAds.sh | jq '.' )
    echo "${ALL_LIST}" | jq 'del(.ads)'
    if [ -n "$(echo ${ALL_LIST} | sed -e '/^[[:space:]]*$/d' )" ]; then
        if [ "null" != "$(echo ${ALL_LIST} | jq --raw-output '.total')" ]; then
            NEW_LIST="${ALL_LIST}" 
        else
            UNBLOCK_URL=$(echo ${ALL_LIST} | jq --raw-output)
            echo ${UNBLOCK_URL}
            curl -L -c ${COOKIES} -b ${COOKIES} ${UNBLOCK_URL} -H "${USER_AGENT}" -H "${ACCEPTED_LANGUAGE}" --compressed -o ${HOME}/return
            sleep 5m
        fi
    fi

    if [ "${NEW_LIST}" != "${PRE_LIST}" ]; then
        MESSAGE=""
        TMP_LIST_OLD=$(mktemp)
        TMP_LIST_NEW=$(mktemp)
        echo "${NEW_LIST}" | jq | grep "list_id" | sort >${TMP_LIST_NEW}
        echo "${PRE_LIST}" | jq | grep "list_id" | sort >${TMP_LIST_OLD}
        DIFF=$(diff -E -Z -b -B ${TMP_LIST_OLD} ${TMP_LIST_NEW})
        echo "${DIFF}"
        
        rm -f ${TMP_LIST_NEW} ${TMP_LIST_OLD}

        ADD_LIST=$(echo "${DIFF}" | grep ">" | sed -e 's/[[:space:]]*>[[:space:]]*"list_id": \([[:digit:]]*\),/\1/' )
        REM_LIST=$(echo "${DIFF}" | grep "<" | sed -e 's/[[:space:]]*<[[:space:]]*"list_id": \([[:digit:]]*\),/\1/' )
        
        for i in ${REM_LIST}; do
            ANNONCE=$(echo "${PRE_LIST}" | jq '."ads"[]' | jq "select(.\"list_id\"==${i})")
            if [ -e "${SAVEPATH}/${i}" ]; then
                date '+%Y-%m-%d %H:%M:%S' >>${SAVEPATH}/${i}/date.removed
                mv "${SAVEPATH}/${i}/ONLINE" "${SAVEPATH}/${i}/OFFLINE"
            fi
        done
        
        for i in ${ADD_LIST}; do
            if [ ! -e "${SAVEPATH}/${i}" ]; then
                mkdir ${SAVEPATH}/${i}
            fi
            ANNONCE=$(curl -b ${COOKIES} "https://api.leboncoin.fr/finder/classified/${i}" -H "${USER_AGENT}" -H 'Accept: */*' -H "${ACCEPTED_LANGUAGE}" --compressed -H "api_key: ${API_KEY}" -H 'Content-Type: application/json' 2>/dev/null )
            echo "${ANNONCE}" >${SAVEPATH}/${i}/ads.json
            date '+%Y-%m-%d %H:%M:%S' >>${SAVEPATH}/${i}/date.added
            if [ "true" == $(echo "${ANNONCE}" | jq '.has_phone') ]; then
                NUMERO_TEL=$(curl -b ${COOKIES} 'https://api.leboncoin.fr/api/utils/phonenumber.json' -H "${USER_AGENT}" -H 'Accept: application/json' -H "${ACCEPTED_LANGUAGE}" --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Connection: keep-alive' --data "app_id=leboncoin_web_utils&key=${JSON_KEY}&list_id=${i}&text=1" 2>/dev/null | jq --raw-output '."utils"."phonenumber"' )
                echo "${NUMERO_TEL}" >>${SAVEPATH}/${i}/telephone
            else
                echo "null" >>${SAVEPATH}/${i}/telephone
                NUMERO_TEL=""
            fi
            
            if [ -e "${SAVEPATH}/${i}/OFFLINE" ]; then
                mv "${SAVEPATH}/${i}/OFFLINE" "${SAVEPATH}/${i}/ONLINE"
            else
                touch "${SAVEPATH}/${i}/ONLINE"
            fi
            
            CMD_IMG=""
            if [ 0 -ne $(echo "${ANNONCE}" | jq '."images"."nb_images"' ) ]; then
                LIST_IMG=$(echo "${ANNONCE}" | jq --raw-output '."images"."urls_large"[]' )
                for j in ${LIST_IMG}; do
                    IMG_NAME=$(echo "${j}" | sed -e 's/^.*\/\([[:alnum:]]*.jpg\)/\1/' )
                    if [ ! -e "${SAVEPATH}/${i}/${PIC_PREFIX}${IMG_NAME}" ]; then
                        curl ${j} -o ${SAVEPATH}/${i}/${PIC_PREFIX}${IMG_NAME} 2>/dev/null
                    fi
                    CMD_IMG="-a ${SAVEPATH}/${i}/${PIC_PREFIX}${IMG_NAME} ${CMD_IMG}"
                done
            fi

            
            PRIX=$(echo "${ANNONCE}" | jq '."price"[0]')
            SURFACE=$(echo "${ANNONCE}" | jq --raw-output '."attributes"[] | select(."key"=="square")."value"' )
            if [ ${SURFACE} -gt 0 ] ; then
                PRIX_M2=$((${PRIX}/${SURFACE}))
            else
                PRIX_M2="NC"
            fi
            URL=$(echo "${ANNONCE}" | jq --raw-output '."url"' )
            SALER=$(echo "${ANNONCE}" | jq --raw-output '."owner"."name"' )
            MESSAGE=$(echo -e "Annonce : ${i}\n${URL}\nTéléphone : ${NUMERO_TEL}\nNom : ${SALER}\n\nPrix : ${PRIX}\nSurface : ${SURFACE}\nPrix au m^2 : ${PRIX_M2}\n---\n\n$(echo "${ANNONCE}" | jq '.body')\n---" )
            SUBJECT="Alerte LBC - $(echo "${ANNONCE}" | jq --raw-output '."subject"' )"
#             echo -e ${MAIL_CLIENT} -n -s "${SUBJECT}" ${CMD_IMG} -S sendcharsets="UTF-8" -S ttycharset="UTF-8" -S from=${EMAIL} -S ${SMTP_STARTTLS} -S smtp=${SMTP_SERVER} -S ${SMTP_AUTH} -S smtp-auth-user="${SMTP_USER}" -S smtp-auth-password="${SMTP_PASS}" ${DESTINATAIRE}
            echo -e "${MESSAGE}" | ${MAIL_CLIENT} -n -s "${SUBJECT}" ${CMD_IMG} -S sendcharsets="UTF-8" -S ttycharset="UTF-8" -S from=${EMAIL} -S ${SMTP_STARTTLS} -S smtp=${SMTP_SERVER} -S ${SMTP_AUTH} -S smtp-auth-user="${SMTP_USER}" -S smtp-auth-password="${SMTP_PASS}" ${DESTINATAIRE}
            ERR="$((${ERR}+${?}))"
        done
        
    fi
    sleep ${REFRESH_TIME}
 
done


rm -f ${TMP_HTML} ${COOKIES}
exit 0
