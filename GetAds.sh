#!/bin/bash

export PATH="${PATH}:/usr/local/bin:/usr/local/games:/usr/games:${HOME}/.local/bin:${HOME}/bin"

cd $(dirname ${0})
source ./VARIABLES

####################
## Initialisation ##
####################

#Surfaces : 20 25 30 35 40 50 60 70 80 90 100 110 120 130 140 150 200 300 500 ou rien
SURFACE_MIN="80"
SURFACE_MAX=""

#Prix :
PRIX_MIN=""
PRIX_MAX="550000"

#Vendeur : ${PARTICULIER} ou ${PROFESSIONNEL} ou vide si les deux.
VENDEUR="${PARTICULIER}"

#Type de bien : MAISON, APPARTEMENT, TERRAIN, PARKING ou AUTRE
BIEN="${MAISON},${APPARTEMENT}"

#Ville de recherche : (ne pas surpprimer les guillements ni les simples quotes.
VILLE='"Strasbourg"'

#Catégorie : ${LOCATION} ou ${VENTE}
TRANSACTION=${VENTE}

####################
## Program Start  ##
####################

hash jq
if [ 0 -ne ${?} ]; then
    echo "jq missing."
    exit
fi

TMP_HTML=$(mktemp)
COOKIES=$(mktemp)


#Touche :
curl -c ${COOKIES} -b ${COOKIES} "https://www.leboncoin.fr" -H "${USER_AGENT}" -H "${ACCEPTED_LANGUAGE}" --compressed -o ${TMP_HTML} 2>/dev/null
#Récupération de APP_CONFIG
APP_CONFIG=$(cat ${TMP_HTML} | sed -e "/[[:space:]]*window\.APP_CONFIG/,/<\/script>/!d" -e "/<\/script>/d" -e "s/^[[:space:]]*window.APP_CONFIG[[:space:]]*=[[:space:]]*//" -e "/^[[:space:]]*$/d" )

API=$(echo "${APP_CONFIG}" | jq '."API"' )
JSON_KEY=$(echo "${API}" | jq --raw-output '."KEY_JSON"' )
API_KEY=$(echo "${API}" | jq --raw-output '."KEY"' )


if [ ! -z ${VENDEUR} ]; then 
    Q_VENDEUR=",\"owner_type\":${VENDEUR}"
fi


Q_BIEN="\"real_estate_type\":[${BIEN}]"

#VILLE='"city":"Strasbourg"'
#LABEL=',"label":"Strasbourg (toute la ville)"'
#AREA=',"area":{"default_radius":10000,"lat":48.56576422523346,"lng":7.737746140498545}'
Q_CITY="\"city\":${VILLE}${LABEL}${AREA}"

#Processing :
if [ ! -z ${SURFACE_MIN} ]; then 
    Q_SURFACE_MIN="\"min\":${SURFACE_MIN}"
fi
if [ ! -z ${SURFACE_MAX} ]; then 
    Q_SURFACE_MAX="\"max\":${SURFACE_MAX}"
fi
if [ ! -z ${Q_SURFACE_MIN} ] || [ ! -z ${Q_SURFACE_MAX} ]; then
    if [ ! -z ${Q_SURFACE_MIN} ] && [ ! -z ${Q_SURFACE_MAX} ]; then
        SURFACE="\"square\":{${Q_SURFACE_MIN},${Q_SURFACE_MAX}}"
    else
        SURFACE="\"square\":{${Q_SURFACE_MIN}${Q_SURFACE_MAX}}"
    fi
fi

if [ ! -z ${PRIX_MIN} ]; then 
    Q_PRIX_MIN="\"min\":${PRIX_MIN}"
fi
if [ ! -z ${PRIX_MAX} ]; then 
    Q_PRIX_MAX="\"max\":${PRIX_MAX}"
fi
if [ ! -z ${Q_PRIX_MIN} ] || [ ! -z ${Q_PRIX_MAX} ]; then
    if [ ! -z ${Q_PRIX_MIN} ] && [ ! -z ${Q_PRIX_MAX} ]; then
        PRIX="\"price\":{${Q_PRIX_MIN},${Q_PRIX_MAX}}"
    else
        PRIX="\"price\":{${Q_PRIX_MIN}${Q_PRIX_MAX}}"
    fi
fi

if [ ! -z ${PRIX} ] || [ ! -z ${SURFACE} ]; then
    if [ ! -z ${PRIX} ] && [ ! -z ${SURFACE} ]; then
        Q_RANGE=",\"ranges\":{${PRIX},${SURFACE}}"
    else
        Q_RANGE=",\"ranges\":{${PRIX}${SURFACE}}"
    fi
fi


RESULTS=$(curl -b ${COOKIES} 'https://api.leboncoin.fr/finder/search' -H "${USER_AGENT}" -H 'Accept: */*' -H "${ACCEPTED_LANGUAGE}" --compressed -H "api_key: ${API_KEY}" -H 'Content-Type: application/json' \
--data "{\"filters\":{\"category\":{\"id\":\"${TRANSACTION}\"},\"enums\":{\"ad_type\":[\"offer\"],${Q_BIEN}},\"keywords\":{},\"location\":{\"locations\":[{\"locationType\":\"city\",${Q_CITY}}]}${Q_RANGE}},${NB_RESULT},\"limit_alu\":0,${OFFSET}${Q_VENDEUR}}"  2>/dev/null )

NB_RESULT="\"limit\":$(echo ${RESULTS} | jq '."total_private"')"
echo "Nombre resultat : $(echo ${RESULTS} | jq '."total_private"' ) " >&2

RESULTS=$(curl -b ${COOKIES} 'https://api.leboncoin.fr/finder/search' -H "${USER_AGENT}" -H 'Accept: */*' -H "${ACCEPTED_LANGUAGE}" --compressed -H "api_key: ${API_KEY}" -H 'Content-Type: application/json' \
--data "{\"filters\":{\"category\":{\"id\":\"${TRANSACTION}\"},\"enums\":{\"ad_type\":[\"offer\"],${Q_BIEN}},\"keywords\":{},\"location\":{\"locations\":[{\"locationType\":\"city\",${Q_CITY}}]}${Q_RANGE}},${NB_RESULT},\"limit_alu\":0,${OFFSET}${Q_VENDEUR}}"  2>/dev/null )


echo "${RESULTS}" | jq '.'


rm -f ${TMP_HTML} ${COOKIES}
exit 0
